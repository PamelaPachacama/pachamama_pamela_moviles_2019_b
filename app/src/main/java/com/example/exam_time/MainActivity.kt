package com.example.exam_time

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var gameScoreTextView: TextView
    private lateinit var tapMeButton: Button
    private lateinit var startButton: Button
    private var gameScore = 0
    private var score = 0
    private lateinit var countDownTimer: CountDownTimer
    private var timeLeft = 0
    private var isGameStarted = false

    private var TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "onCreate called, score is $gameScore, timer is $timeLeft")

        gameScoreTextView = findViewById(R.id.score_text_view)
        tapMeButton = findViewById(R.id.button_timer)
        startButton = findViewById(R.id.start_button)

        startButton.setOnClickListener{startGame()}

        tapMeButton.setOnClickListener{incrementScore()}

        if(savedInstanceState != null){
            gameScore = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            isGameStarted = savedInstanceState.getBoolean(GAME_STARTED)
            restoreGame()
        }else{
            resetGame()
        }


    }





    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SCORE_KEY,gameScore)
        outState.putInt(TIME_LEFT_KEY,timeLeft)



    }

    override fun onDestroy() {
        super.onDestroy()

    }

    private fun incrementScore(){

        if(!isGameStarted){
            startGame()
        }
        score = (0..10).shuffled().first()
        if(score == timeLeft){
            gameScore = 100
            gameScoreTextView.text = getString(R.string.score, gameScore)
        }
        if((score- 1) == timeLeft ){
            gameScore = 50
            gameScoreTextView.text = getString(R.string.score, gameScore)

        }
        if((score +1) == timeLeft ){
            gameScore = 50
            gameScoreTextView.text = getString(R.string.score, gameScore)

        }
        else{
            endGame()
        }

    }
    private fun resetGame(){
        gameScore = 0
        gameScoreTextView.text = getString(R.string.score, gameScore)

        timeLeft = (0..10).shuffled().first()

        isGameStarted = false

    }
    private fun startGame(){
        timeLeft = (0..10).shuffled().first()
        isGameStarted= true

    }
    private fun endGame(){
        Log.d(TAG, "onCreate called, score is $gameScore, timer is $timeLeft")
        Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_LONG).show()
        resetGame()

    }

    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score, gameScore)


        if(isGameStarted){
            timeLeft = (0..10).shuffled().first()

        }

    }



    companion object{
        private const val SCORE_KEY = "SCORE_KEY"
        private const val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        private const val GAME_STARTED = "GAME_STARTED"
    }
}
